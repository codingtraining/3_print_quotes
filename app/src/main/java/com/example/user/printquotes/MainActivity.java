package com.example.user.printquotes;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final EditText firstAnswerEdit = (EditText) findViewById(R.id.first_answer_edit);
        final EditText secondAnswerEdit = (EditText) findViewById(R.id.second_answer_edit);
        final TextView secondQuestionText = (TextView) findViewById(R.id.second_question_text);
        final TextView finalText = (TextView) findViewById(R.id.final_text);

        firstAnswerEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                secondQuestionText.setVisibility(View.INVISIBLE);
                secondAnswerEdit.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                secondQuestionText.setVisibility(View.VISIBLE);
                secondAnswerEdit.setVisibility(View.VISIBLE);
            }
        });

        secondAnswerEdit.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                finalText.setVisibility(View.INVISIBLE);
            }

            @Override
            public void afterTextChanged(Editable editable) {
                finalText.setVisibility(View.VISIBLE);
                finalText.setText(secondAnswerEdit.getText().toString() + " says, " + "\"" + firstAnswerEdit.getText().toString() + ".\"");
            }
        });
    }
}
